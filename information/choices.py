from django.utils.translation import ugettext_lazy as _

TYPE_DOCUMENT = (
    ('cc', _('Cédula de ciudadanía')),
    ('passport', _('Pasaporte')),
    ('dni', _('DNI')),
    ('TI', _('Tarjeta de Identidad')),
)

TYPE_GENDER = (
    ('male', _('Masculino')),
    ('female', _('Femenino')),
)
