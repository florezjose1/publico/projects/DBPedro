from django.contrib import admin

from information.models import Department, Municipality, InformationUser


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ('name',)
    search_fields = ('name',)


@admin.register(Municipality)
class MunicipalityAdmin(admin.ModelAdmin):
    list_display = ('name', 'department')
    list_filter = ('department',)
    search_fields = ('department__name', 'name',)


@admin.register(InformationUser)
class InformationUserAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'identification', 'gender', 'phone', 'municipality')
    list_filter = ('municipality__name', 'gender', 'type_document')
    search_fields = ('first_name', 'last_name', 'identification', 'gender', 'phone', 'municipality__name',)
